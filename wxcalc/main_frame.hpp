// ==================================================================== //

// Copyright (C) 2021 RethiSoft group

// This file is part of wxCalculator. wxCalculator is free software: you
// can redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// wxCalculator is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with wxCalculator. If not, see <https://www.gnu.org/licenses/>.

// ==================================================================== //

/**
 * \file    wxcalc/main_frame.hpp
 * \brief   Main wxWidgets frame (window) of wxCalculator
 * \author  Adam Maleszka <adam_maleszka@aol.com>
 */

#ifndef WXCALC_MAIN_FRAME_HPP_
#define WXCALC_MAIN_FRAME_HPP_

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

// Separate wxCalculator from the default namespace
namespace wxcalc {

/*************************************************************************
 * \class   MainFrame
 * \brief   Main frame (window) of wxCalculator
 ************************************************************************/
class MainFrame : public wxFrame {
 public:
  /**
   * \brief   Constructor of wxCalculator window
   *
   * At this moment, it only creates an empty frame with an `Exit` button
   * in the menu bar.
   */
  MainFrame();

 private:
  /**
   * \brief       Closes the frame thus ends the program
   * \param[in]   The event that closes the window
   */
  void OnExit(wxCommandEvent& event);
};

};  // namespace wxcalc

#endif  // WXCALC_MAIN_FRAME_HPP_
