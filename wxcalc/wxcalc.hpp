// ==================================================================== //

// Copyright (C) 2021 RethiSoft group

// This file is part of wxCalculator. wxCalculator is free software: you
// can redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// wxCalculator is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with wxCalculator. If not, see <https://www.gnu.org/licenses/>.

// ==================================================================== //

/**
 * \file      wxcalc/wxcalc.hpp
 * \brief     Main header of wxCalculator front-end
 * \author    Adam Maleszka <adam_maleszka@aol.com>
 *
 * This front-end provides efficient but powerful graphical interface for
 * wxCalculator. It is based on wxWidgets.
 */

#ifndef WXCALC_WXCALC_HPP_
#define WXCALC_WXCALC_HPP_

#include <wx/wxprec.h>

#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wxcalc/main_frame.hpp>

// Separate wxCalculator from the default namespace
namespace wxcalc {

/*************************************************************************
 * \class     wxCalc
 * \brief     Main class of the wxCalculator front-end
 *
 * This is the starting point of the wxWidgets application. It works
 * similar to the `main` function of the regular software but in C# style.
 ************************************************************************/
class wxCalc : public wxApp {
 public:
  /**
   * \brief   Initializes wxWidgets application
   * \return  Status whether initialization was successful
   */
  virtual bool OnInit();
};

}  // namespace wxcalc

#endif  // WXCALC_WXCALC_HPP_
