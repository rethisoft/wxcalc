// ==================================================================== //

// Copyright (C) 2021 RethiSoft group

// This file is part of wxCalculator. wxCalculator is free software: you
// can redistribute it and/or modify it under the terms of the GNU General
// Public License as published by the Free Software Foundation, either
// version 3 of the License, or (at your option) any later version.

// wxCalculator is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License along
// with wxCalculator. If not, see <https://www.gnu.org/licenses/>.

// ==================================================================== //

#include <wxcalc/main_frame.hpp>

namespace wxcalc {

MainFrame::MainFrame() : wxFrame(NULL, wxID_ANY, "Empty") {
  // Menu bar: File
  wxMenu *menuFile = new wxMenu;
  menuFile->Append(wxID_EXIT);

  wxMenuBar *menuBar = new wxMenuBar;
  menuBar->Append(menuFile, "&File");
  SetMenuBar(menuBar);

  // Bind events
  Bind(wxEVT_MENU, &MainFrame::OnExit, this, wxID_EXIT);
}

void MainFrame::OnExit(wxCommandEvent &event) { Close(true); }

}  // namespace wxcalc
