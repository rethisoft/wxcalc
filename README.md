# wxCalculator

A simple calculator based on wxWidgets. Created for educational purposes.

<!--
TODO: Write a more developed description before the first release
## Description

Let people know what your project can do specifically. Provide context and
add a link to any reference visitors might be unfamiliar with. A list of
Features or a Background subsection can also be added here. If there are
alternatives to your project, this is a good place to list differentiating
factors.
-->

## Installation

### Requirements

General requirements are:

- [cmake](https://cmake.org/) (3.13 or later) — for buildsystem generation
- [doxygen](https://www.doxygen.nl/index.html) (1.8.16 or later) — for technical documentation

### Building

wxCalculator can be built using CMake:

```
mkdir build; cd build
cmake -G "Unix Makefiles" ..
make -j$(nproc)
```

You can replace `"Unix Makefiles"` with any build system, e.g.:

```
cmake -G "Visual Studio 15"
```

For a complete list of supported build systems by your platform, execute:

```
cmake --help
```

There is possibility to specify build type also:

```
cmake -DCMAKE_BUILD_TYPE=Release ..
```

Building types are: `Debug`, `RelWithDebInfo`, `Release` and `MinSizeRel`.

<!--
TODO: Complete usage info before the first release
## Usage

Use examples liberally, and show the expected output if you can. It's
helpful to have inline the smallest example of usage that you can
demonstrate, while providing links to more sophisticated examples if they
are too long to reasonably include in the README.

TODO: List all features to be implemented (checklist)e
## Roadmap
-->

## Contributing

Please read the global
[CONTRIBUTING.md](https://gitlab.com/rethisoft/documents/-/blob/main/CONTRIBUTING.md).

The RethiSoft group is **always open** to new contributors. If you would
be willing, please **contact [me](https://gitlab.com/maleszka)** (owner)
via email: <adam_maleszka@aol.com>.

## License

This project is under GPLv3 license. Full content can be found in the
[LICENSE](LICENSE) file.
