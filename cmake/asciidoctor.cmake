include(FindPackageHandleStandardArgs)

#########################
#  Asciidoctor package  #
#########################

# Find the executable
find_program(Asciidoctor_EXECUTABLE NAMES asciidoctor)
find_program(Asciidoctor_EXECUTABLE_PDF NAMES asciidoctor-pdf)

# Create Asciidoctor package
find_package_handle_standard_args(Asciidoctor REQUIRED_VARS
  Asciidoctor_EXECUTABLE Asciidoctor_EXECUTABLE_PDF)

# Hide cached variable
if (Asciidoctor_FOUND)
  mark_as_advanced(Asciidoctor_EXECUTABLE)
  mark_as_advanced(Asciidoctor_EXECUTABLE_PDF)
endif()

# Expose the executables to the build system
if (Asciidoctor_FOUND AND NOT TARGET asciidoctor AND NOT TARGET
    asciidoctor-pdf)
  add_executable(asciidoctor IMPORTED)
  set_property(TARGET asciidoctor PROPERTY IMPORTED_LOCATION
    ${Asciidoctor_EXECUTABLE})

  add_executable(asciidoctor-pdf IMPORTED)
  set_property(TARGET asciidoctor-pdf PROPERTY IMPORTED_LOCATION
    ${Asciidoctor_EXECUTABLE_PDF})
endif()

########################
#  Asciidoctor macros  #
########################

# Create target for all AsciiDoc documents
add_custom_target(docs-asciidoc)

# Create a HTML article
macro (asciidoc_html file)
  set(input_dir ${CMAKE_CURRENT_SOURCE_DIR})
  set(output_dir ${CMAKE_CURRENT_BINARY_DIR})

  set(input_adoc ${input_dir}/${file}.adoc)
  set(output_html ${output_dir}/${file}.html)

  add_custom_command(
    OUTPUT ${output_html}
    COMMAND ${Asciidoctor_EXECUTABLE}
      -o ${output_html}
      ${input_adoc}
    MAIN_DEPENDENCY
      ${input_adoc}
    WORKING_DIRECTORY
      ${output_dir}
    COMMENT
      "Generating ${file}.html"
  )

  add_custom_target(docs-asciidoc-html-${file}
    DEPENDS
      ${output_html}
  )

  add_dependencies(docs-asciidoc docs-asciidoc-html-${file})
endmacro (asciidoc_html)

# Create a PDF document using asciidoctor-pdf
macro (asciidoc_pdf file)
  set(input_dir ${CMAKE_CURRENT_SOURCE_DIR})
  set(output_dir ${CMAKE_CURRENT_BINARY_DIR})

  set(input_adoc ${input_dir}/${file}.adoc)
  set(output_pdf ${output_dir}/${file}.pdf)

  add_custom_command(
    OUTPUT ${output_pdf}
    COMMAND ${Asciidoctor_EXECUTABLE_PDF}
    -o ${output_pdf}
    ${input_adoc}
    MAIN_DEPENDENCY
      ${input_adoc}
    WORKING_DIRECTORY
      ${output_dir}
    COMMENT
      "Generating ${file}.pdf"
  )

  # Create final target
  add_custom_target(docs-asciidoc-pdf-${file}
    DEPENDS
      ${output_pdf}
  )
  add_dependencies(docs-asciidoc docs-asciidoc-pdf-${file})
endmacro(asciidoc_pdf)
